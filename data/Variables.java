package scripts.red_WoadLeafBuyer.data;

import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

public class Variables {
	private boolean run = true;
	private boolean getNewTask = true;
	private int failSafeCount;
	private String stage = "BUYING_LEAF";
	private String farmerName = "Wyson the gardener";
	private String leafName = "Woad leaf";
	private int leafCount;
	private boolean guiState = true;
	private int mouseSpeed = 100;
	private int closedDoorID = 24050;
	
	private final RSArea HOUSE_AREA = new RSArea(new RSTile[]{new RSTile(3027,3381), new RSTile(3026,3382),new RSTile(3027,3383),
															new RSTile(3031,3083), new RSTile(3031,3375), new RSTile(3027,3375)});

	
	//GUI
	private int maxBuy = 200;
	//

	public boolean run() {
		// TODO Auto-generated method stub
		return run;
	}
	public void setRun(boolean b){
		run = b;
	}
	public void setGetNewTasks(boolean b) {
		// TODO Auto-generated method stub
		
	}
	public boolean getNewTasks(){
		return getNewTask;
	}
	public boolean areWeStuck(){
		return failSafeCount > 20;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage){
		this.stage = stage;
	}
	public int getMaxBuy(){
		return maxBuy;
	}
	public void setMaxBuy(int max){
		maxBuy = max;
	}
	public String getFarmerName(){
		return farmerName;
	}
	public String getLeafName(){
		return leafName;
	}
	public int getLeafCount() {
		return leafCount;
	}
	public void setLeafCount(int leafCount) {
		this.leafCount = leafCount;
	}
	public void incramentLeafCount(int by){
		leafCount += by;
	}
	public boolean guiOn() {
		return guiState;
	}
	public void setGUI(boolean b){
		guiState = b;
	}
	public int getMouseSpeed() {
		// TODO Auto-generated method stub
		return mouseSpeed;
	}
	public void setMouseSpeed(int mouseSpeed){
		this.mouseSpeed = mouseSpeed;
	}
	public int getClosedDoorID(){
		return closedDoorID;
	}
	public boolean inHouseArea(RSTile tile){
		return HOUSE_AREA.contains(tile);
	}
	public boolean inTheSameArea(RSTile player, RSTile wyson){	
		return (HOUSE_AREA.contains(player) == HOUSE_AREA.contains(wyson));
		
	}

	

}

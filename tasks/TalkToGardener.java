package scripts.red_WoadLeafBuyer.tasks;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.NPCChat;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.types.RSNPC;

import scripts.redUtil.antiBan.AntiBan;


public class TalkToGardener extends Task{
	private boolean DEBUG = false;
	private TaskManager manager;

	private RSNPC[] wyson;
	
	public TalkToGardener(TaskManager manager){
		this.manager = manager;
	}

	@Override
	public void execute() {
		clickOnGardener();
		
	}

	private boolean clickOnGardener() {

		wyson = NPCs.find(manager.variables.getFarmerName());// find the gardener
		if(wyson.length > 0 && wyson[0] != null){
			if(!DynamicClicking.clickRSNPC(wyson[0], "Talk-to"))//click on him
				return false;
			AntiBan.waitUntilIdle(500, 1000);//waits until player is not moving(just in case gardener is a few steps away)
			Timing.waitCondition(() -> NPCChat.getMessage() != null, General.random(3000, 4000));
		}
		return true;
		
	}


	public boolean validate() {

		if(DEBUG) {
			General.println(this.getClass().toString()+ ": " + (NPCChat.getMessage() == null &&
							NPCChat.getOptions() == null));
		}
		return (NPCChat.getMessage() == null &&
				NPCChat.getOptions() == null);
	}

	@Override
	public int priority() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String status() {
		// TODO Auto-generated method stub
		return "Clicking on Gardener";
	}

}
